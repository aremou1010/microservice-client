/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.agrosfer.pdm.microserviceclient.configs;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 *
 * @author g3a
 */
@Component
@ConfigurationProperties(prefix = "ms-client-configs")
@Data
public class ApplicationPropertiesConfig {
    
    String url;
    String msCodificationCreateUrl;
    
}

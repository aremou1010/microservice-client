/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.agrosfer.pdm.microserviceclient.web.controller;

import java.net.URI;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Optional;
import org.agrosfer.pdm.microserviceclient.beans.ReferenceBean;
import org.agrosfer.pdm.microserviceclient.configs.ApplicationPropertiesConfig;
import org.agrosfer.pdm.microserviceclient.dao.ClientDAO;
import org.agrosfer.pdm.microserviceclient.model.Client;
import org.agrosfer.pdm.microserviceclient.model.ClientStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author g3a
 */
@RestController
public class ClientController {

    @Autowired
    ClientDAO clientDAO;

    @Autowired
    ApplicationPropertiesConfig apc;

    @Autowired
    RestTemplate restTemplate;

    @PostMapping("/clients")
    public ResponseEntity<?> createClient(@RequestBody Client client) {

        //client.setCompanyName(null);
        client.setCreateAt(Timestamp.from(Instant.now()));
        client.setUpdateAt(Timestamp.from(Instant.now()));
        client.setIsDelete(Boolean.FALSE);
        client.setStatut(ClientStatus.INACTIVE);

        Optional<Client> findAClient;
        if (client.getIsCompany()) {
            client.setCompanyName(client.getCompanyName().toLowerCase());
            findAClient = clientDAO.findByCompanyName(client.getCompanyName().toLowerCase());
            if (findAClient.isPresent()) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Client cannot be created. Company name already exist.");
            }
        } else {
            client.setCompanyName(null);
        }

        findAClient = clientDAO.findByPhone(client.getPhone());
        if (findAClient.isPresent()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Client cannot be created. Phone number already exist.");
        }

        String ref = "";
        ref = getReference(Client.class.getSimpleName());

        if (ref == "") {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Client cannot be created. Go to check microservice-codification");
        }

        client.setReference(ref);
        Client newClient = clientDAO.save(client);

        if (newClient != null) {
            return ResponseEntity.accepted()
                    .location(URI.create(apc.getUrl() + "/clients/" + newClient.getReference()))
                    .body(newClient);
        }

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Client cannot be created");

    }

    @GetMapping("/clients")
    public ResponseEntity<?> getClients() {
        return ResponseEntity.ok(clientDAO.findAll());
    }
    
    @GetMapping("/clients/company")
    public ResponseEntity<?> getClientsCompany() {
        return ResponseEntity.ok(clientDAO.findAllByIsCompanyTrue());
    }
    
    @GetMapping("/clients/individual")
    public ResponseEntity<?> getClientsIndividual() {
        return ResponseEntity.ok(clientDAO.findAllByIsCompanyFalse());
    }

    @GetMapping("/clients/{ref}")
    public ResponseEntity<?> getClientByRef(@PathVariable("ref") String ref) {

        Optional<Client> oneClient;
        oneClient = clientDAO.findByReferenceAndIsDeleteFalse(ref);

        if (oneClient.isPresent()) {
            return ResponseEntity.of(oneClient);
        }

        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body("This client does not exist");

    }

    @GetMapping("/clients/id/{id}")
    public ResponseEntity<?> getClientById(@PathVariable("id") Long id) {

        Optional<Client> oneClient;
        oneClient = clientDAO.findById(id);

        if (oneClient.isPresent()) {
            return ResponseEntity.of(oneClient);
        }

        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body("This client does not exist");

    }

    @GetMapping("/clients/phone/{attribute}")
    public ResponseEntity<?> getClientByPhone(@PathVariable("attribute") String attribute) {

        Optional<Client> oneClient;
        oneClient = clientDAO.findByPhone(attribute);

        if (oneClient.isPresent()) {
            return ResponseEntity.of(oneClient);
        }

        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body("This client does not exist");

    }

    @GetMapping("/clients/company/{attribute}")
    public ResponseEntity<?> getClientByCompany(@PathVariable("attribute") String attribute) {

        Optional<Client> oneClient;
        oneClient = clientDAO.findByCompanyName(attribute);

        if (oneClient.isPresent()) {
            return ResponseEntity.of(oneClient);
        }

        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body("This client does not exist");

    }

//    @PutMapping("/clients")
    @PostMapping("/clients/update")
    public ResponseEntity<?> updateClient(@RequestBody Client client) {

        Optional<Client> oneClient = clientDAO.findByReferenceAndIsDeleteFalse(client.getReference());

        if (oneClient.isPresent()) {

            if (!oneClient.get().getPhone().equals(client.getPhone()) && clientDAO.findByPhone(client.getPhone()).isPresent()) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("INCORRECT PHONE");
            }

            if (client.getIsCompany() && !oneClient.get().getCompanyName().equals(client.getCompanyName()) && clientDAO.findByCompanyName(client.getCompanyName()).isPresent()) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("INCORRECT COMPANY");
            }

            oneClient.get().setAddress(client.getAddress());
            oneClient.get().setBirthDate(client.getBirthDate());
            oneClient.get().setEmail(client.getEmail());
            oneClient.get().setWebsite(client.getWebsite());
            oneClient.get().setFirstName(client.getFirstName());
            oneClient.get().setLastName(client.getLastName());
            oneClient.get().setFieldsOfActivity(client.getFieldsOfActivity());
            oneClient.get().setFunctions(client.getFunctions());

            oneClient.get().setCompanyName(client.getCompanyName());
            oneClient.get().setPhone(client.getPhone());

            oneClient.get().setUpdateAt(Timestamp.from(Instant.now()));
        }else{
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("NOT FOUND");
        }

        Client updatedClient = clientDAO.saveAndFlush(oneClient.get());

        if (updatedClient != null) {
            return ResponseEntity.accepted()
                    .location(URI.create(apc.getUrl() + "/clients/" + updatedClient.getReference()))
                    .body(updatedClient);
        }

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Client cannot be updated");

    }

    public String getReference(String entityName) {
        ReferenceBean rb = new ReferenceBean();
        rb.setEntity(entityName);

        //Method1
        HttpEntity<?> request = new HttpEntity<>(rb);
        ResponseEntity<ReferenceBean> re;
        try {
            re = restTemplate.exchange(URI.create(apc.getMsCodificationCreateUrl()), HttpMethod.POST, request, ReferenceBean.class);
            return re.getBody().getCode();

        } catch (RestClientException e) {
            return ("");
        }

        //Method2
        //ReferenceBean newR = restTemplate.postForObject(URI.create(apc.getMsCodificationCreateUrl()), rb, ReferenceBean.class);
        //return ResponseEntity.ok(newR);
    }
}

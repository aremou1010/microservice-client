/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.agrosfer.pdm.microserviceclient.dao;

import java.util.List;
import java.util.Optional;
import org.agrosfer.pdm.microserviceclient.model.Client;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author g3a
 */
public interface ClientDAO extends JpaRepository<Client, Long> {
    
    Optional<Client> findByReferenceAndIsDeleteFalse(String ref);
    
    Optional<Client> findByPhone(String phone);
    
    Optional<Client> findByCompanyName(String companyName);
    
    List<Client> findAllByIsCompanyTrue();
    
    List<Client> findAllByIsCompanyFalse();
    
}

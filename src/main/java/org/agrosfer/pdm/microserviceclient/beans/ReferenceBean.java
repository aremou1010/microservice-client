/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.agrosfer.pdm.microserviceclient.beans;

import lombok.Data;

/**
 *
 * @author g3a
 */
@Data
public class ReferenceBean {
    
    private Long id;
    
    private String entity;
    
    private String code;

    public ReferenceBean() {
    }

    public ReferenceBean(Long id, String entity, String code) {
        this.id = id;
        this.entity = entity;
        this.code = code;
    }
    
    
}

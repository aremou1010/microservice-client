/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.agrosfer.pdm.microserviceclient.model;

/**
 *
 * @author g3a
 */
public enum ClientStatus {
    
    ACTIVE, INACTIVE;
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.agrosfer.pdm.microserviceclient.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import lombok.Data;

/**
 *
 * @author g3a
 */
@Data
@Entity
public class Client {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    
    private String reference;
    
    private String firstName;
    
    private String lastName;
    
    @Column(unique = true)
    private String phone;
    
    private String address;
    
    
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date birthDate;
    
    private String email;
    
    private String website;
    
    private Boolean isCompany;
    
    @Column(unique = true)
    private String companyName;
    
    @ElementCollection
    private List<String> fieldsOfActivity;
    
    @ElementCollection
    private List<String> functions;
    
    private ClientStatus statut;
    
    private Timestamp createAt;
    
    private Timestamp updateAt;
    
    private Timestamp deleteAt;
    
    private Boolean isDelete;

    public Client() {
    }

    public Client(Long id, String reference, String firstName, String lastName, String phone, String address, Date birthDate, String email, String website, Boolean isCompany, String companyName, ClientStatus statut, Timestamp createAt, Timestamp updateAt, Timestamp deleteAt, Boolean isDelete) {
        this.id = id;
        this.reference = reference;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phone = phone;
        this.address = address;
        this.birthDate = birthDate;
        this.email = email;
        this.website = website;
        this.isCompany = isCompany;
        this.companyName = companyName;
        this.statut = statut;
        this.createAt = createAt;
        this.updateAt = updateAt;
        this.deleteAt = deleteAt;
        this.isDelete = isDelete;
    }
    
}
